--pour utiliser ce script, il faut être dans la situation écran appartient aux spaces et pas spaces appartient aux écrans.
-- Ceci est défini dans Préférences Système -> Mission control -> désélectionner la boîte: Les écrans disposent de Spaces distincts

-- pour trouver le nom exact des ecrans : dans la console hammerspoon saisir hs.screen("INDICE_SUR_LE_NOM_DE_L_ECRAN") 
-- Vous pouvez remplacer INDICE_SUR_LE_NOM_DE_L_ECRAN par la première lettre de la marque par exemple
-- Si 2 écrans ont le même nom car même modèle, alors il faut renseigner leur UUID que l'on trouve en exécutant le code si dessous dans la console
for idx_ecran, ecran in pairs(hs.screen.allScreens()) do
  print("--------")
  print(ecran:name())
  print(ecran:getUUID())
end
print("--------")

spacesSpoon = require("hs._asm.undocumented.spaces")
require("functions")
require("midi")
print('--------- construction des configurations et detection de la configuration actuelle----------')
local portable = "Color LCD"
local iiyama1 = "8D032E33-F5B6-08EE-F493-E2791E10A3BC"
local iiyama2 = "B014F405-5592-BCC9-C884-B7F4BCCA54E9"
local ecran_MAO = "Philips 273EL"
-- local ptite_tele = "PHILIPS FTV" -- je l'ai enlevée

-- configuration_possible doit contenir le nom des écrans de chaque configuration et doit être rempli de concert avec la liste suivante (liste_config_ecran_space_fenetre)
local configurations_possible = {
  -- maison confinement
  {
    iiyama1, iiyama2, ecran_MAO, portable
  },
  -- arca confinement / Modartt
  {
    iiyama2, iiyama1, portable
  },
  -- portable seul
  {
    portable
  }
}

-- on imbrique 4 tableaux, la config, les écrans dans le sens de la configuration correspondante de la liste précédente (Configurations_possible), puis les spaces, et enfin la liste des fenêtres.
-- la syntaxe est la suivante: titre ou partie du titre##application::options
-- l'application n'est pas obligatoire, ni le titre mais au moins un des 2. Surtout si on a le titre exacte, pas besoin de mettre l'application
-- pour trouver les titres / noms d'application, j'ai créé une fonction qui renvoie ces info dans la console hammerspoon: shift + ctrl + cmd + i
-- Pour ouvrir un eapplication avec le raccourci correspondant, il faut mettre le nom de l'application, pas seulement le titre de la fenêtre
-- pour les options:
-- - contains: si on a mis qu'un morceau du titre
-- - remaining: si on s'intéresse majoritairement à une feêtre et que l'on place les autres ailleurs sans intérêt particulier.
-- - windowX: la fenètre X. En sachant qu'on a pas de moyen de savoir quel fenêtre sera en 1, 2, 3...(passe après contains incompatible avec remaining)
-- - NoOpen: on n'ouvrira pas cette application par le raccourci "^ + shift + cmd + o"
-- - NoClose on ne fermera pas l'application avec le raccourci "^ + shift + cmd + c"
-- -
-- - default: on ne touche pas à la position/taille
-- - 50L la moitié de l'écran gauche
-- - 50R la moitié de l'écran droite
-- - TopLeft le quart en haut à gauche
-- - TopRight BottomLeft BottomRight

-- pour utiliser ce script, voir la liste des keybidings en bas du fichier:
-- "^ + shift + cmd + o" : ouvrir la liste des applications définies
-- "^ + shift + cmd + c" : fermer la liste des applications définies
-- "^ + shift + cmd + l" : placer les fenêtres
-- "^ + shift + cmd + i" : information sur le titre exact et l'appliccation d'une fenêtre
-- "^ + shift + cmd + p" : déplacer la fenêtre qui a le focus sur le space précédent
-- "^ + shift + cmd + n" : déplacer la fenêtre qui a le focus sur le space suivant
-- "shift + f1" : déplacer le focus sur le space 1
-- "shift + f2" : déplacer le focus sur le space 2 et comme ça jusqu'à 12

local liste_config_ecran_space_fenetre ={
  -- config 1
  {
    -- iiyama1
    {
      -- space 1
      {"##Firefox::window1", "##Finder"},
      -- space 2
      {"##PDF Expert", "##Firefox::window2"},
      -- space 3
      {"##Firefox::window3", "##MidiPipe::default"}
    },
    -- écran Principal (iiyama2)
    {
      -- space 1
      {"MODARTT##Code::contains", "Pianoteq MORPH v6.7.0 Options::default", "Spectrum Profile::default"},
      -- space 2
      {"##Spark"},
      -- space 3
      {"##Live::NoOpen", "##Max::NoOpen", "##Code::remaining"}
    },
    -- écran MAO
    {
      -- space 1
      {"Pianoteq MORPH v6.7.0##Pianoteq 6 MORPH::default", "Pianoteq Top Secret debug panel::default", "Hammerspoon Console::NoOpen-default", "##MidiVelocityLocker::default"},
      -- space 2
      {"##Slack"},
      -- space 3
      {"Loopback::TopLeft", "New Blank Session##Audio Hijack::NoOpen-BottomLeft", "Audio Hijack##Audio Hijack::NoOpen-BottomLeft", "Lecteur multimédia VLC::BottomRight", "##Terminal::NoOpen-TopRight"}
    },
    -- écran portable
    {
      -- space 1
      {"Sonogram::default"},
      -- space 2
      {"##Messages::50L", "##WhatsApp::50R"},
      -- space 3
      {"##iTunes", "Calendrier::default-NoOpen", "Intel Power Gadget::NoOpen-default", "Organteq v1.0.5##organteq 1::NoOpen-default"}
    }
  },
-- config 2
  {
    -- iiyama1
    {
      -- space 1
      {"##Firefox::window1", "##Finder"},
      -- space 2
      {"##Slack", "##PDF Expert", "##Firefox::window2"},
      -- space 3
      {"##Firefox::window3", "##MidiPipe::default"}
    },
    -- écran Principal (iiyama2)
    {
      -- space 1
      {"MODARTT##Code::contains", "Pianoteq MORPH v6.7.0 Options::default", "Spectrum Profile::default"},
      -- space 2
      {"##Spark"},
      -- space 3
      {"##Live::NoOpen", "##Max::NoOpen", "##Code::remaining"}
    },
    -- écran portable
    {
      -- space 1
      {"Sonogram::default", "Pianoteq MORPH v6.7.0##Pianoteq 6 MORPH::default", "Pianoteq Top Secret debug panel::default", "Hammerspoon Console::NoOpen-default", "##MidiVelocityLocker::default"},
      -- space 2
      {"##Messages::50L", "##WhatsApp::50R"},
      -- space 3
      {"##iTunes", "Calendrier::default-NoOpen", "Intel Power Gadget::NoOpen-default", "Organteq v1.0.5##organteq 1::NoOpen-default", "Loopback::TopLeft", "New Blank Session##Audio Hijack::NoOpen-BottomLeft", "Audio Hijack##Audio Hijack::NoOpen-BottomLeft", "Lecteur multimédia VLC::BottomRight", "##Terminal::NoOpen-TopRight"}
    }
  },
  -- config 3
  {
    -- écran portable
    {
      -- space 1
      {"##Finder::remaining-default", "##Firefox::window1", "##Firefox::window2", "##Firefox::window3"},
      -- space 2
      {"##Code"},
      -- space 3
      {"Pianoteq v6.7.0 Options::default", "Spectrum Profile::default", "Sonogram::default", "Pianoteq v6.7.0##Pianoteq 6 MORPH::default", "Pianoteq Top Secret debug panel::default", "##MidiVelocityLocker::default"},
      -- space 4
      {"##Messages::50L", "##WhatsApp::50R", "##PDF Expert", "##Slack", "##Spark"},
      -- space 5
      {"Hammerspoon Console::NoOpen-default", "##iTunes", "Calendrier::default", "Intel Power Gadget::default", "##Organteq::default"}
    }
  }
}

function Init()

  local compteur_config = {}
  local nombre_de_space = {}
  local uuid_ecran = {}
  local nombre_de_space_total_par_ecran = {}
  local position_absolue_ecrans = {}
  local point_origine = hs.geometry.point(0, 0)
  -- on initialise le compteur utile a la detection de config automatique ainsi que le nombre d'espace par ecran par config utilisé plus tard
  for idx_config, config in pairs(configurations_possible) do
    nombre_de_space[idx_config] = {}
    uuid_ecran[idx_config] = {}
    position_absolue_ecrans[idx_config] = {}
    compteur_config[idx_config] = #config
    print("fitting score initial de la config " .. idx_config .. "= " .. #config)
  end

  print('-------- attribution du fitting score et remplissage de liste utiles pour la suite ---------------')
  for idx_config, config in pairs(configurations_possible) do
    if #config == #hs.screen.allScreens() then
      for index, screenObj in pairs(hs.screen.allScreens()) do
        nombre_de_space_total_par_ecran[idx_config] = 0
        print("config numéro " .. idx_config)
        for idx_ecran, ecran in pairs(config) do
          nombre_de_space[idx_config][idx_ecran] = #liste_config_ecran_space_fenetre[idx_config][idx_ecran]
          if #liste_config_ecran_space_fenetre[idx_config][idx_ecran] > nombre_de_space_total_par_ecran[idx_config] then
            nombre_de_space_total_par_ecran[idx_config] = #liste_config_ecran_space_fenetre[idx_config][idx_ecran]
          end
          if screenObj:name() == ecran or screenObj:getUUID() == ecran then -- on peut soit donner un nom d'écran ou un UUID
            print(ecran)
            print(idx_ecran)
            print(screenObj:name())
            print(screenObj:getUUID())
            uuid_ecran[idx_config][idx_ecran] = screenObj:getUUID()
            position_absolue_ecrans[idx_config][idx_ecran] = screenObj:localToAbsolute(point_origine)
            compteur_config[idx_config] = compteur_config[idx_config] - 1
          end
        end
      end
    end
  end

  print("------------- print et attribution de l'index de la configuration actuelle ----------------")
  local idx_config_actuelle = 0
  for idx_config, config_fitting_score in pairs(compteur_config) do
    print('config : ' .. idx_config)
    print('fitting score (0 is perfect): ' .. compteur_config[idx_config])
    if compteur_config[idx_config] == 0 then
      for idx_ecran, ecran in pairs(configurations_possible[idx_config]) do
        print('nombre de space requis pour l ecran ' .. ecran .. ' = ' .. nombre_de_space[idx_config][idx_ecran])
        print('l uuid de l ecran ' .. ecran .. ' est = ' .. uuid_ecran[idx_config][idx_ecran])
      end
      idx_config_actuelle = idx_config
    end
  end

  return uuid_ecran[idx_config_actuelle], nombre_de_space_total_par_ecran[idx_config_actuelle], liste_config_ecran_space_fenetre[idx_config_actuelle], configurations_possible[idx_config_actuelle]
end

-- ce qui suit est dans une fonction car est associé à une touche du clavier
function Placement()
  local uuids, nb_space_total, config , config_ecran = Init()

  print('------------ ajustement du nombre de space maintenant que l on connait la config (tous les ecrans partage ont les space synchrones dans mission control sinon ça ne fonctionne pas) ---------')
  print('------------ il va aussi falloir bouger les fenêtres qui sont sur les spaces à supprimer ---------------------')
  local idx_ecran_actuel = 0
  local titre_cache = {} -- le titre de chaque fenetre des applications dont on ne connait pas le titre des fenetres
  local compteur_application_fenetre = {} -- pour les application dont on ne peut pas connaitre le titre a l'avance (Firefox)
  for ecran_uuid, liste_space in pairs(spacesSpoon.layout()) do
    print(ecran_uuid)
    -- trouve l'index de l'ecran actuel
    for idx_uuid, uuid in pairs(uuids) do
      if uuid == ecran_uuid then idx_ecran_actuel = idx_uuid end
    end

    local difference_space_voulu_existant = nb_space_total - #liste_space -- le + 1 pour avoir un espace poubelle
    -- s'il n'y a pas assez de space pas de probleme on en crée. S'il y en a trop, pas de problèmes non plus on déplace les fenêtres puis on les supprimera
    if difference_space_voulu_existant > 0 then
      for i = 1,difference_space_voulu_existant do
        print('space created')
        spacesSpoon.createSpace(ecran_uuid)
      end
    end
    local spaces_id_reverse = spacesSpoon.query("allSpaces") -- elle sort à l'envers...
    SpacesId = {}
    for i=#spaces_id_reverse, 1, -1 do
      SpacesId[#SpacesId+1] = spaces_id_reverse[i]
    end

    for idx_space, space_id in pairs(liste_space) do
      print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%% " .. space_id)
      for idx_fenetre, fenetre in pairs(spacesSpoon.allWindowsForSpace(space_id)) do
        print(fenetre)
        print(fenetre:application():name())
        print('++++++++++++++++++++++++++++++++')
        -- remarque:
        -- I only have a very few number of spaces now that I share space between screen. So I only need to send the windows to the right space and 
        -- after place it using a layout guessed accordingto the table liste_config_ecran_space_fenetre

        -- a decommenter quand les fonctions sont pretes
        -- check if the window exist in the config:
        local idx_space_fenetre, backend = FaitPartiDeLaConfig(fenetre, config, compteur_application_fenetre)
        if cle ~= nil then titre_cache[backend.cle] = backend.val end
        if app ~= nil then compteur_application_fenetre[backend.app] = backend.compteur end
        if idx_space_fenetre then
          print("oui" .. idx_space_fenetre)
          -- if true: check if in the right space
          local nombre_de_spaces_fenetre = fenetre:spaces()
          if #nombre_de_spaces_fenetre == 1 then
            if idx_space_fenetre ~= idx_space then spacesSpoon.moveWindowToSpace(fenetre:id(), SpacesId[idx_space_fenetre]) end
          end
        else
          print("non")
          -- if not send it to the last space
          print(SpacesId[#SpacesId])
          print(fenetre:id())
          -- if true: check if in the right space
          local nombre_de_spaces_fenetre = fenetre:spaces()
          if #nombre_de_spaces_fenetre == 1 then
            spacesSpoon.moveWindowToSpace(fenetre:id(), SpacesId[#SpacesId])
          end
          print(fenetre:title())
        end
        print('xxxxxxxxxxxxxxxxxxxxxxxx')
      end
    end
  end

  -- a decommenter quand la fonction= est prete
  -- pour space layout on applique un layout pour chaque space
  Layouts = {}
  for idx_space, space_id in pairs(SpacesId) do
    if idx_space <= nb_space_total then
      spacesSpoon.changeToSpace(SpacesId[idx_space])
      Layouts[idx_space] = ObtenirLayout(idx_space, config , config_ecran, titre_cache)
      hs.layout.apply(Layouts[idx_space])
      print("done space" .. idx_space)
      print("--------------------------")
    else
      print(idx_space)
      -- spacesSpoon.removeSpace(space_id)
    end
  end
  -- to get to the first space
  spacesSpoon.changeToSpace(SpacesId[1])
  Sleep(1)
  -- hs.notify.show("Hammerspoon", "Les fenètres sont organisées!")
  hs.notify.new(nil,{
    autoWithdraw = true,
    title = "Hammerspoon",
    informativeText = "Les fenêtres sont organisées!",
    soundName = "Hero"
  }):send()
  -- hs.alert.show("écrans et space organisés!")
  print("-=========================")
  for idxL, L in pairs(Layouts) do
    for idxLi, Li in pairs(L) do      
      for idxF, info in pairs(Li) do
        print(info)
      end
    end
    print("SPACE DONEEEEEEEEEEEEE")
  end
  print("x++++++++=xxxxxx+++++++xxxxxx")
end

local uuids, nb_space_total, config , config_ecran = Init()


hs.hotkey.bind({"ctrl", "cmd", "shift"}, "o", "opening Applications", function() OpenApps(config) end, nil, nil)
hs.hotkey.bind({"ctrl", "cmd", "shift"}, "c", "closing Applications", function() CloseApps(config) end, nil, nil)
hs.hotkey.bind({"ctrl", "cmd", "shift"}, "l", "placing Applications", Placement, nil, nil)
hs.hotkey.bind({"ctrl", "cmd", "shift"}, "n", "moving to next space", MoveNextSpace, nil, nil)
hs.hotkey.bind({"ctrl", "cmd", "shift"}, "p", "moving to previous space", MovePreviousSpace, nil, nil)
hs.hotkey.bind({"ctrl", "cmd", "shift"}, "i", "printing winfow info",GetFenetreInfo, nil, nil)
App = "MATLAB"
hs.hotkey.bind({"ctrl", "cmd", "shift"}, "f", "next window of the concerned app",function() NextPreviousWindow(App, "croissant") end, nil, nil)
hs.hotkey.bind({"ctrl", "cmd", "shift"}, "d", "previous window of the concerned app",function() NextPreviousWindow(App, "decroissant") end, nil, nil)

for i = 1, 12 do
  hs.hotkey.bind("shift", "f" .. i, "move to space " .. i, function() Move2Space(i) end, nil, nil)
end
