
function ParseFenetreInfo(fenetresInfo)
  -- fonction qui lit les données liées aux fenêtres rentrées par l'utilisateur
  -- entrée: chaîne de caractère
  -- sorties: 3 chaînes de caractère ou nil correspondant à l'application, info sur la fenètre, options potentielles
  local title = nil
  local app = nil
  local options = nil
  if string.find(fenetresInfo, "##") == nil then
    if string.find(fenetresInfo, "::") == nil then
      title = fenetresInfo
    else -- on a que les ::
      _, _, title, options = string.find(fenetresInfo, "(.+)::(.+)")
    end
  else
    if string.find(fenetresInfo, "::") == nil then
      _, _, title, app = string.find(fenetresInfo, "(.+)##(.+)")
      if title == nil then -- ca commence direct par ##
        _, _, app = string.find(fenetresInfo, "##(.+)")
      end
    else -- on a les :: et ##
      _, _, title, app, options = string.find(fenetresInfo, "(.+)##(.+)::(.+)")
      if title == nil then -- ca commence direct par ##
        _, _, app, options = string.find(fenetresInfo, "##(.+)::(.+)")
      end
    end
  end
  return title, app, options
end

function IsRemaining(fenetre_app, config)
  result = true
  for idx_ecran, fenetre_ecrans in pairs(config) do
    for idx_spaces, fenetre_spaces in pairs(fenetre_ecrans) do
      for idx_fenetre, fenetresInfo in pairs(fenetre_spaces) do
        local title, app, options = ParseFenetreInfo(fenetresInfo)
        if fenetre_app:application():name() == app then
          if string.find(options, "contains") then
            if string.find(fenetre_app:title(), title) then return false end
          end
        end
      end
    end
  end
  return true
end

function FaitPartiDeLaConfig(fenetre, config, compteur_application_fenetre)
  -- permet de savoir si une fenetre fait partie de la config
  -- entrées: l'objet fenetre à étudier, la config où chercher, les compteurs conservés dans compteur_application_fenetre dans le cas de l'utilisation de l'option windowX
  -- sortie: false ou int pour le space correspondant à la fenêtre, un dictionnaire nommé backend pour le fonctionnement de la fonction principale placement
  local backend = {}
  for idx_ecran, fenetre_ecrans in pairs(config) do
    for idx_spaces, fenetre_spaces in pairs(fenetre_ecrans) do
      for idx_fenetre, fenetresInfo in pairs(fenetre_spaces) do
        print(fenetresInfo)
        local title, app, options = ParseFenetreInfo(fenetresInfo)
        if app == nil then
          if options ~= nil then
            if string.find(options, "contains") then
              if string.find(fenetre:title(), title) then backend.cle = title .. "##" .. app; backend.val = fenetre:title(); return idx_spaces, backend end
            else
              if fenetre:title() == title then return idx_spaces, backend end
            end
          else
            if fenetre:title() == title then return idx_spaces, backend end
          end
        else
          if fenetre:application():name() == app then
            if title == nil then
              if options ~= nil then
                if string.find(options, "window") then
                  _, _, idx_fenetre = string.find(options, "window(%d)-*")
                  print("on est a l index : " .. idx_fenetre)
                  if compteur_application_fenetre[app] == nil or compteur_application_fenetre[app] < tonumber(idx_fenetre) then -- on a deja fait toutes les allocations, c'est une fenêtre en plus
                    local compteur
                    if compteur_application_fenetre[app] == nil then
                      compteur = 1
                    else
                      compteur = compteur_application_fenetre[app] + 1
                    end
                    backend.cle = idx_fenetre .. "##" .. app; backend.val = fenetre:title()
                    backend.app = app; backend.compteur = compteur
                    return idx_spaces, backend
                  end
                elseif string.find(options, "remaining") then
                  backend.cle = "##" .. app
                  backend.val = {}
                  fenetres_de_app = fenetre:application():allWindows()
                  print(#fenetres_de_app)
                  for idx_fenetre, fenetre_app in pairs(fenetres_de_app) do
                    if IsRemaining(fenetre_app, config) then
                      backend.val[#backend.val + 1] = fenetre_app:title()
                      print(fenetre_app:title())
                    end
                  end
                  if #backend.val > 0 then
                    return idx_spaces, backend
                  else
                    return false, backend
                  end
                else
                  return idx_spaces, backend
                end
              else
                return idx_spaces, backend
              end
            else
              if options ~= nil then
                if string.find(options, "contains") then
                  if string.find(fenetre:title(), title) then backend.cle = title .. "##" .. app; backend.val = fenetre:title(); return idx_spaces, backend end
                else
                  if fenetre:title() == title then return idx_spaces, backend end
                end
              else
                if fenetre:title() == title then return idx_spaces, backend end
              end
            end
          end
        end
      end
    end
  end
  return false, backend
end

function AddFenetreAuLayout(application, titre, ecran, options)
  -- fonction interne qui ajout des élément à la liste layout comme le désire la spoon layout de hammerspoon en fonction de mes options
  if options == nil then
    return {application, titre, hs.screen.find(ecran), hs.layout.maximized, nil, nil}
  elseif string.find(options, "default") then
    return {application, titre, hs.screen.find(ecran), nil, nil, nil}
  elseif string.find(options, "50L") then
    return {application, titre, hs.screen.find(ecran), hs.layout.left50, nil, nil}
  elseif string.find(options, "50R") then
    return {application, titre, hs.screen.find(ecran), hs.layout.right50, nil, nil}
  elseif string.find(options, "TopLeft") then
    return {application, titre, hs.screen.find(ecran), hs.geometry.unitrect(0, 0, 0.5, 0.5), nil, nil}
  elseif string.find(options, "TopRight") then
    return {application, titre, hs.screen.find(ecran), hs.geometry.unitrect(0.5, 0, 0.5, 0.5), nil, nil}
  elseif string.find(options, "BottomLeft") then
    return {application, titre, hs.screen.find(ecran), hs.geometry.unitrect(0, 0.5, 0.5, 0.5), nil, nil}
  elseif string.find(options, "BottomRight") then
    return {application, titre, hs.screen.find(ecran), hs.geometry.unitrect(0.5, 0.5, 0.5, 0.5), nil, nil}
  else
    return {application, titre, hs.screen.find(ecran), hs.layout.maximized, nil, nil}
  end
end

function ObtenirLayout(idx_space, config, config_ecran, titre_cache)
  -- crée le tableau layout pour le placement des fenêtres dans un space donné.
  -- entrée: le space que l'on veut agencer, la config à obtenir, les noms des écrans, les titres de fenètres que l'on ne connait pas rempli automatiquement par FaitPartiDeLaConfig dans la fonction principale Placement
  -- sortie: le layout
  local idx_fenetre = 1
  local layout = {}
  for idx_ecran, fenetre_ecrans in pairs(config) do
    if fenetre_ecrans[idx_space] ~= nil then
      for idx_inutile, fenetresInfo in pairs(fenetre_ecrans[idx_space]) do
        -- print(fenetresInfo)
        local title, app, options = ParseFenetreInfo(fenetresInfo)
        if app == nil then
          layout[idx_fenetre] = AddFenetreAuLayout(nil, title, config_ecran[idx_ecran], options)
          idx_fenetre = idx_fenetre + 1
        else
          if title == nil then
            if option ~= nil then
              if string.find(options, "remaining") then
                for idx_titre, titre in pairs(titre_cache["##" .. app]) do
                  -- print("### " .. titre)
                  layout[idx_fenetre] = AddFenetreAuLayout(app, titre, config_ecran[idx_ecran], options)
                  idx_fenetre = idx_fenetre + 1
                end
              else
                layout[idx_fenetre] = AddFenetreAuLayout(app, nil, config_ecran[idx_ecran], options)
                idx_fenetre = idx_fenetre + 1
              end
            else
              layout[idx_fenetre] = AddFenetreAuLayout(app, nil, config_ecran[idx_ecran], options)
              idx_fenetre = idx_fenetre + 1
            end
          else
            if options ~= nil then
              if string.find(options, "contains") then
                layout[idx_fenetre] = AddFenetreAuLayout(app, titre_cache[title .. "##" .. app], config_ecran[idx_ecran], options)
                idx_fenetre = idx_fenetre + 1
              elseif string.find(options, "window") then
                local _, _, idx_fenetre_dans_app = string.find(options, "window(%d)-*")
                layout[idx_fenetre] = AddFenetreAuLayout(app, titre_cache[idx_fenetre_dans_app .. "##" .. app], config_ecran[idx_ecran], options)
                idx_fenetre = idx_fenetre + 1
              else
                layout[idx_fenetre] = AddFenetreAuLayout(app, title, config_ecran[idx_ecran], options)
                idx_fenetre = idx_fenetre + 1
              end
            else
              layout[idx_fenetre] = {app, title, config_ecran[idx_ecran], hs.layout.maximized, nil, nil}
              idx_fenetre = idx_fenetre + 1
            end
          end
        end
      end
    end
  end
  for idx2, window in pairs(layout) do print(window) for idx3, ele in pairs(window) do print(ele) end end
  return layout
end


local clock = os.clock
function Sleep(n)  -- secondes (en float si on veut)
  local t0 = clock()
  while clock() - t0 <= n do end
end

function OpenApps(config)
  -- ouvre les applications de la config qui n'ont pas l'option noOpen
  for idx_ecran, ecran in pairs(config) do
    for idx_space, spaces in pairs(ecran) do
      for idx_fenetreInfo, fenetresInfo in pairs(spaces) do
        local title, app, options = ParseFenetreInfo(fenetresInfo)
        if options ~= nil then
          if string.find(options, "NoOpen") == nil then
            if app ~= nil then
              if app == "Code" then
                hs.application.open("Visual Studio Code")
              else
                hs.application.open(app)
              end
              Sleep(1)
            end
          end
        else
          if app ~= nil then
            if app == "Code" then
              hs.application.open("Visual Studio Code")
            else
              hs.application.open(app)
            end
            Sleep(1)
          end
        end
      end
    end
  end
  hs.notify.new(nil,{
    autoWithdraw = false,
    title = "Hammerspoon",
    informativeText = "Applications ouvertes!",
    soundName = "Hero"
  }):send()
end

function CloseApps(config)
  -- ferme les applications de la config qui n'ont pas l'option noClose
  for idx_ecran, ecran in pairs(config) do
    for idx_space, spaces in pairs(ecran) do
      for idx_fenetreInfo, fenetresInfo in pairs(spaces) do
        local title, app, options = ParseFenetreInfo(fenetresInfo)
        if option ~= nil then
          if string.find(options, "NoClose") == nil then
            print(app)
            Sleep(0.5)
            if app ~= nil then
              print("." .. app .. ".")
              appObj = hs.application.get(app)
              if appObj ~= nil then
                print('-----------')
                print(appObj:name())
                appObj:kill()
              end
            end
          end
        else
          print(app)
          Sleep(0.5)
          if app ~= nil then
            print("." .. app .. ".")
            appObj = hs.application.get(app)
            if appObj ~= nil then
              print('-----------')
              print(appObj:name())
              appObj:kill()
            end
          end
        end
      end
    end
  end
  hs.notify.new(nil,{
    autoWithdraw = false,
    title = "Hammerspoon",
    informativeText = "Applications fermées!",
    soundName = "Hero"
  }):send()
end

function MoveNextSpace()
  -- permet de faire bouger la fenetre active d'un space a l'autre. Nécessaire pour travailler avec 2 fenêtres qui sont originalement sur 2 spaces différents
  local spaces_id_reverse = spacesSpoon.query("allSpaces") -- elle sort à l'envers...
  SpacesId = {}
  for i=#spaces_id_reverse, 1, -1 do
    SpacesId[#SpacesId+1] = spaces_id_reverse[i]
  end
  local fenetre = hs.window.focusedWindow()
  print(fenetre:title())
  local fenetre_space = fenetre:spaces()
  for idx_space, space in pairs(SpacesId) do
    if fenetre_space[1] == space then
      if idx_space < #SpacesId then
        spacesSpoon.moveWindowToSpace(fenetre:id(), SpacesId[idx_space + 1])
        spacesSpoon.changeToSpace(SpacesId[idx_space + 1])
        return true
      else
        return true
      end
    end
  end
end
function MovePreviousSpace()
  -- permet de faire bouger la fenetre active d'un space a l'autre. Nécessaire pour travailler avec 2 fenêtres qui sont originalement sur 2 spaces différents
  local spaces_id_reverse = spacesSpoon.query("allSpaces") -- elle sort à l'envers...
  SpacesId = {}
  for i=#spaces_id_reverse, 1, -1 do
    SpacesId[#SpacesId+1] = spaces_id_reverse[i]
  end
  local fenetre = hs.window.focusedWindow()
  print(fenetre:title())
  local fenetre_space = fenetre:spaces()
  for idx_space, space in pairs(SpacesId) do
    if fenetre_space[1] == space then
      if idx_space > 1 then
        spacesSpoon.moveWindowToSpace(fenetre:id(), SpacesId[idx_space - 1])
        spacesSpoon.changeToSpace(SpacesId[idx_space - 1])
        return true
      else
        return true
      end
    end
  end
end

function Move2Space(idx)
  -- permet de faire bouger la fenetre active d'un space a l'autre. Nécessaire pour travailler avec 2 fenêtres qui sont originalement sur 2 spaces différents
  local spaces_id_reverse = spacesSpoon.query("allSpaces") -- elle sort à l'envers...
  SpacesId = {}
  for i=#spaces_id_reverse, 1, -1 do
    SpacesId[#SpacesId+1] = spaces_id_reverse[i]
  end
  spacesSpoon.changeToSpace(SpacesId[idx])
  hs.alert.show("Space " .. idx)

end

function GetFenetreInfo()
  -- permet de faire bouger la fenetre active d'un space a l'autre. Nécessaire pour travailler avec 2 fenêtres qui sont originalement sur 2 spaces différents
  local fenetre = hs.window.focusedWindow()
  print('-------------------')
  print("titre: " .. fenetre:title())
  print("app: " .. fenetre:application():name())
  print("id : " .. fenetre:id())
  hs.notify.new(nil,{
    autoWithdraw = false,
    title = "Hammerspoon",
    informativeText = "informations imprimées dans la console Hammerspoon!",
    soundName = "Hero"
  }):send()
end

function NextPreviousWindow(app, sens)
  local obj_app = hs.application.get(app)
  local fen_list = obj_app:allWindows()
  local focused_fenetre = obj_app:focusedWindow()
  print('-------------------')
  local ids = {}
  for idx_fen, fenetre in pairs(fen_list) do
    print("titre: " .. fenetre:title())
    table.insert(ids, fenetre:id())
  end
  if sens == "croissant" then
    table.sort(ids)
  elseif sens == "decroissant" then
    table.sort(ids, function(a, b) return a > b end)
  end
  table.insert(ids, ids[1])
  print('-------------------')
  local fenetre_actuelle = 0
  for idx_id, id in pairs(ids) do
    print(id)
    if fenetre_actuelle == 1 then
      hs.window.find(id):focus()
      return
    end
    if focused_fenetre:id() == id then
      fenetre_actuelle = 1
    end
    print(fenetre_actuelle)
  end
end