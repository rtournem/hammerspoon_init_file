Installation
============

1. télécharger la dernière release de Hammerspoon: [Hammerspoon-XXX.zip](https://github.com/Hammerspoon/hammerspoon/releases/tag/0.9.78)
2. la dezipper dans le dossier /Application
3. lancer hammerspoon pour déclencher les autorisations d'accès
4. (ajouter si vous le souhaitez hammerspoon aux programmes à lancer au démarrage)
5. télécharger la dernière release de l'API de gestion des spaces: [spaces-XXX.tar.gz](https://github.com/asmagill/hs._asm.undocumented.spaces/releases)
6. la décompresser dans le dossier ```~/.hammerspoon``` (ce qui devrait créer un arbre de cette forme: ```~/.hammerspoon/hs/_asm...```)
7. cloner ce repo dans le dossier ```~/.hammerspoon``` (ce qui devrait ajouter les deux seuls fichiers ```init.lua``` et ```functions.lua``` dans le dossier ```~/.hammerspoon```)

Utilisation
===========

Pour utiliser ce script, il faut être dans la situation écran appartient aux spaces et pas spaces appartient aux écrans.
Ceci est défini dans Préférences Système -> Mission control -> désélectionner la boîte: Les écrans disposent de Spaces distincts.

Si vous êtes avec votre portable seul sans second écran, il est probable que vous puissiez tester directement le script avec mes raccourcis. Pour cela il suffit d'ouvrir la console hammerspoon avec le menu en forme de petit marteau dans la barre de tâche puis: ```shift + f1``` pour aller sur le space 1, ```shift + f2``` pour aller sur le space 2 et ainsi de suite jusqu'à 12. ```shift + ctrl + cmd + o``` pour ouvrir les applications que j'ai définies et/ou ```shift + ctrl + cmd + l``` pour les placer. 

Pour créer vos configs, il faut définir au début du fichier init.lua:
1. des listes d'écrans selon les configurations (Généralement on en a 2: portable seul et portable avec son écran secondaire),
2. des listes de fenêtres par écran par space selon les configurations.

J'ai mis dans le fichier lua comment remplir ces deux listes nommées: ```configurations_possible``` et ```liste_config_ecran_space_fenetre```.

En bas du fichiers, il y a les raccourcis qui permettent de gérer les quelques actions du script.

On ouvre la console hammerspoon, on ```Reload config``` et on peut utiliser les raccourcis! Enjoy!

N'hésitez pas si vous avez des remarques / problèmes.